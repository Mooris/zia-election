#ifndef __AMODULE_HPP__
# define __AMODULE_HPP__

# include "ARequest.hpp"

class	AModule {
public:
	virtual ~AModule();

	virtual void prepareRequest() = 0; /* You need to provide a route, a method and eventually parameters and a body request by calling ARequest's setters */

	virtual void executeRequest() = 0; /* Execute the request by calling the execRequest method from the ARequest class. The _request->execRequest method will automaticly fill the ARequest attribute.
					      You can get this attribute by calling the ARequest getter*/

	virtual void sendResponse() = 0; /* Get the response from ARequest class and send it to the client */

private:
	ARequest _request;
}

#endif /* __AMODULE_HPP__ */
