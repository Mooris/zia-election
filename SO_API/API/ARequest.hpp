#ifndef __AREQUEST_HPP__
# define __AREQUEST_HPP__

# include <string>

class ARequest {
public:
	enum StatusCode {
		OK = 200,
		ERROR = 400,
		NOT_FOUND = 404,
		SERVER_ERROR = 500,
		CONNECTION_TIMEOUT = 504
	};
	enum Method {
		POST,
		GET
	};
	virtual ~IRequest();

	/* You need to provide a route, a method and eventually paramerters and a body request to call this->execRequest() */

	virtual void setRequestRoute(std::string &requestRoute) = 0; /* Example: www.mywebsite.com/login */
        virtual std::string &getRequestRoute() const = 0;

	virtual void setMethod(Method method) = 0; /* Using Method Enum */
        virtual Method &getMethod() const = 0;

	virtual void setParameters(std::string &parameters) = 0; /* Example: ?toto=tata&titi=42 */
        virtual std::string &getParameters() const = 0;

	virtual void setBodyRequest(std::string &bodyRequest) = 0; /* Exemple: {"param1": "val1", "param2": 42}*/
        virtual std::string &getBodyRequest() const = 0;

	virtual void execRequest() = 0; /* execRequest will fill the status and the response attribut */

	/* Once this->execRequest() is call, you can get the status and the response */

	virtual void setStatus(StatusCode code) = 0;
        virtual StatusCode getStatus() const = 0;

	virtual void setResponse(std::string &response) = 0;
        virtual std::string &getResponse() const = 0;

protected:
	std::string _requestRoute;
	Method _method;
	std::string _parameters;
	std::string _bodyRequest;

	StatusCode _code;
	std::string _response;

};

#endif /* !__AREQUEST_HPP__ */
