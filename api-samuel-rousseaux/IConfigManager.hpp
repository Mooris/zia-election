class IConfigManager
{
public:
  virtual ~IConfigManager() {}

  /*
  Returns true if could read such value from config file
  If not, writes the default 'def' to to the config file and return false
  */
  virtual bool GetValue(const std::string &name, const std::string &def, std::string &out) const = 0;

  /*
  Returns true if could read such value from config file
  If not, writes the default 'def' to to the config file and return false
  */
  virtual bool GetValue(const std::string &name, int def, int &out) const = 0;
};
