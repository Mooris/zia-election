class ISocket
{
public:
  virtual ~ISocket(void) {};

  virtual int GetID(void) const = 0;

  virtual void Send(const void *buf, const size_t size) = 0;
  virtual void Recv(void *buf, const size_t size) = 0;
};
