#pragma once

#include <string>
#include <vector>

class IModuleInterface
{
public:
  virtual ~IModuleInterface(void) {};

  /*
  Called by the core when this module is requested to reload
  */
  virtual void Reload() = 0;

  /*
  Called in the thread of the current client
  Returns true if the event has been handled, false otherwise
  */
  virtual bool OnEvent(ISocket &sock, const std::string &eventName, std::unordered_map<std::string, std::string> &params) = 0;

  /*
  Returns the list of all events that this module handles
  */
  virtual std::vector<std::string> GetEventsList(void) const = 0;

  /*
  Called right after loading of modules to allow modules to read from configuration
  */
  virtual void OnPostLoad(IConfigManager &mgr) = 0;

  /*
  Returns the port number this module listens to or -1 if none
  Forcely called after OnPostLoad
  */
  virtual int GetPort() const = 0;

  /*
  Returns a new socket to use by all other modules bound to the raw TCP client socket ID
  Return nullptr if this module is not intended to load receive client connections
  Ex: A SSL/TLS module might use this to hook a custom read/write function to decode bytes
  */
  virtual std::unique_ptr<ISocket> NewSocket(int rawsockid) = 0;
};

#ifdef WIN32

/*
To add at the end of the main module source file
*/
#define IMPLEMENT_MODULE(name)                          \
extern "C"                                              \
{                                                       \
  __declspec(dllexport) IModuleInterface *ZIA_Load()    \
  {                                                     \
    return (new name());                                \
  }                                                     \
}

#else

/*
To add at the end of the main module source file
*/
#define IMPLEMENT_MODULE(name)                          \
extern "C"                                              \
{                                                       \
  IModuleInterface *ZIA_Load()                          \
  {                                                     \
    return (new name());                                \
  }                                                     \
}

#endif
