class MyModule : public IModuleInterface
{
private:
  std::ofstream _stream;
  std::mutex _mutex;
public:
  MyModule()
    : _stream("./my_log_file.txt")
  {
  }

  void Reload()
  {
  }

  bool OnEvent(ISocket &, const std::string &, std::unordered_map<std::string, std::string> &)
  {
    _mutex.lock();
    _stream << eventparam;
    _mutex.unlock();
  }

  std::vector<std::string> GetEventsList(void) const
  {
    std::vector<std::string> res = {
      "LOG"
    };
    return (res);
  }

  int GetPort(IConfigManager &) const
  {
    return (-1);
  }

  std::unique_ptr<ISocket> NewSocket(int rawsockid)
  {
    return (nullptr);
  }

  void OnPostLoad(IConfigManager &)
  {
  }
};

IMPLEMENT_MODULE(MyModule);
