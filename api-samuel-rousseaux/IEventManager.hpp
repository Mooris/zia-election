/*
Each client has new thread created and an EventManager is created inside that thread
*/
class IEventManager
{
public:
  virtual ~IEventManager(void) {};

  virtual void SendEvent(const std::string &eventName, /* A définir */) = 0;
};
